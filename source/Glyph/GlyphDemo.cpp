#include "GlyphDemo.hpp"
#include "GlyphDrawable.hpp"
#include "GlyphShaderBitmap.hpp"
#include "GlyphShaderSDF.hpp"
#include "GlyphTexture.hpp"

#include <glad/gl.h>

#include <imgui.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <memory>
#include <string>
#include <utility>
#include <vector>

const std::string kFontFile = "assets/fonts/fragment-mono/FragmentMono-Regular.ttf";

GlyphDemo::GlyphDemo() :
    CG::Platform::OpenGLWindow(4, 6, 1, "Glyph Demo"),
    mTechnique(Technique::Bitmap),
    mCodepoint(0x61),
    mColor(1.0f, 1.0f, 1.0f, 1.0f),
    mTransform(glm::identity<glm::mat4>()),
    mBitmapTextureInvalidated(false),
    mBitmapFilter(GlyphTexture::Filter::Nearest),
    mBitmapOutput(GlyphShaderBitmap::Output::AlphaBlend),
    mBitmapTextureSize(16),
    mSDFTextureInvalidated(false),
    mSDFFilter(GlyphTexture::Filter::Nearest),
    mSDFTextureSize(64),
    mSDFEdgeThreshold(0.50f),
    mSDFEdgeSoftness(0.005f),
    mSDFEnableOutline(false),
    mSDFOutlineThreshold(0.55f),
    mSDFOutlineSoftness(0.005f),
    mSDFOutlineColor(0.1f, 0.1f, 0.1f, 1.0f)
{}

bool GlyphDemo::OnCreate()
{
    SetFramerateFixed(true);

    mBitmapDrawable = GlyphDrawable::Create();
    if (mBitmapDrawable->IsNull())
    {
        return false;
    }
    mBitmapShader = GlyphShaderBitmap::Create();
    if (mBitmapShader->IsNull())
    {
        return false;
    }
    mBitmapTexture = GlyphTexture::CreateBitmap(mCodepoint, mBitmapTextureSize, kFontFile);
    if (mBitmapTexture->IsNull())
    {
        return false;
    }
    mBitmapDrawable->SetGeometry(mBitmapTexture->GetWidth(), mBitmapTexture->GetHeight());

    mSDFDrawable = GlyphDrawable::Create();
    if (mSDFDrawable->IsNull())
    {
        return false;
    }
    mSDFShader = GlyphShaderSDF::Create();
    if (mSDFShader->IsNull())
    {
        return false;
    }
    mSDFTexture = GlyphTexture::CreateSDF(mCodepoint, mSDFTextureSize, kFontFile);
    if (mSDFTexture->IsNull())
    {
        return false;
    }
    mSDFDrawable->SetGeometry(mSDFTexture->GetWidth(), mSDFTexture->GetHeight());

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    return true;
}

bool GlyphDemo::OnUpdate(
    double pCurrentTime,
    double pElapsedTime)
{
    glClear(GL_COLOR_BUFFER_BIT);

    const float lFramebufferWidthF = static_cast<float>(GetFramebufferWidth());
    const float lFramebufferHeightF = static_cast<float>(GetFramebufferHeight());

    // For small font sizes it is important to snap the text to the pixel grid.
    const int lFramebufferCenterX = static_cast<int>(0.5f * lFramebufferWidthF);
    const int lFramebufferCenterY = static_cast<int>(0.5f * lFramebufferHeightF);
    glm::vec3 lCenter = glm::vec3(lFramebufferCenterX, lFramebufferCenterY, 0.0f);
    glm::mat4 lTranslation = glm::translate(glm::identity<glm::mat4>(), lCenter);

    if (mBitmapTextureInvalidated)
    {
        mBitmapTexture = GlyphTexture::CreateBitmap(mCodepoint, mBitmapTextureSize, kFontFile);
        if (mBitmapTexture->IsNull())
        {
            return false;
        }
        mBitmapDrawable->SetGeometry(mBitmapTexture->GetWidth(), mBitmapTexture->GetHeight());
    }
    mBitmapTexture->SetFilter(mBitmapFilter);
    mBitmapShader->Use();
    mBitmapShader->SetColor(mColor);
    mBitmapShader->SetTransform(lTranslation * mTransform);
    mBitmapShader->SetProjection(glm::ortho(0.0f, lFramebufferWidthF, 0.0f, lFramebufferHeightF));
    mBitmapShader->SetOutput(mBitmapOutput);

    if (mSDFTextureInvalidated)
    {
        mSDFTexture = GlyphTexture::CreateSDF(mCodepoint, mSDFTextureSize, kFontFile);
        if (mSDFTexture->IsNull())
        {
            return false;
        }
        mSDFDrawable->SetGeometry(mSDFTexture->GetWidth(), mSDFTexture->GetHeight());
    }
    mSDFTexture->SetFilter(mSDFFilter);
    mSDFShader->Use();
    mSDFShader->SetColor(mColor);
    mSDFShader->SetTransform(lTranslation * mTransform);
    mSDFShader->SetProjection(glm::ortho(0.0f, lFramebufferWidthF, 0.0f, lFramebufferHeightF));
    mSDFShader->SetEdgeThreshold(mSDFEdgeThreshold);
    mSDFShader->SetEdgeSoftness(mSDFEdgeSoftness);
    mSDFShader->SetOutlineEnabled(mSDFEnableOutline);
    mSDFShader->SetOutlineThreshold(mSDFOutlineThreshold);
    mSDFShader->SetOutlineSoftness(mSDFOutlineSoftness);
    mSDFShader->SetOutlineColor(mSDFOutlineColor);

    switch (mTechnique)
    {
    case Technique::Bitmap:
        mBitmapTexture->Bind(0);
        mBitmapShader->Use();
        mBitmapDrawable->Draw();
        break;
    case Technique::SDF:
        mSDFTexture->Bind(0);
        mSDFShader->Use();
        mSDFDrawable->Draw();
        break;
    }

    ProcessUI();
    return true;
}

void GlyphDemo::OnDestroy()
{
    mBitmapTexture.reset();
    mBitmapShader.reset();
    mBitmapDrawable.reset();
    mSDFTexture.reset();
    mSDFShader.reset();
    mSDFDrawable.reset();
}

void GlyphDemo::OnKeyPressed(
    CG::Platform::OpenGLWindow::KeyboardKey pKey,
    CG::Platform::OpenGLWindow::KeyboardMods pMods)
{
    if (pKey == CG::Platform::OpenGLWindow::KeyboardKey::KEY_F11)
    {
        SetFullScreen(!IsFullScreen());
    }
}

void GlyphDemo::ProcessUI()
{
    mBitmapTextureInvalidated = false;
    mSDFTextureInvalidated = false;

    ImGui::Begin("Settings");

    if (ImGui::DragScalar("Unicode", ImGuiDataType_U32, &mCodepoint, 0.2f, nullptr, nullptr, "0x%08X"))
    {
        mBitmapTextureInvalidated = true;
        mSDFTextureInvalidated = true;
    }

    ImGui::ColorEdit4("Color", glm::value_ptr(mColor));

    static float sScale = 1.0f;
    ImGui::DragFloat("Scale", &sScale, 0.05f, 0.1f, 10.0f, "%.3f");
    static float sRotation = 0.0f;
    ImGui::DragFloat("Rotation", &sRotation, 0.5f, 0.0f, 360.0f, "%.3f");
    mTransform = glm::identity<glm::mat4>();
    mTransform = glm::rotate(mTransform, glm::radians(sRotation), glm::vec3(0.0f, 0.0f, 1.0f));
    mTransform = glm::scale(mTransform, glm::vec3(sScale, sScale, 1.0f));

    static size_t sTechniqueIndex = 0;
    static const std::vector<std::pair<std::string, Technique>> sTechniqueItems =
    {
        { "Bitmap", Technique::Bitmap },
        { "Signed Distance Field", Technique::SDF }
    };
    if (ImGui::BeginCombo("Technique", sTechniqueItems[sTechniqueIndex].first.c_str()))
    {
        for (size_t lIndex = 0; lIndex < sTechniqueItems.size(); lIndex++)
        {
            const bool lSelected = (sTechniqueIndex == lIndex);
            if (ImGui::Selectable(sTechniqueItems[lIndex].first.c_str(), lSelected))
            {
                sTechniqueIndex = lIndex;
                mTechnique = sTechniqueItems[lIndex].second;
            }
            if (lSelected)
            {
                ImGui::SetItemDefaultFocus();
            }
        }
        ImGui::EndCombo();
    }

    if (mTechnique == Technique::Bitmap)
    {
        if (ImGui::DragInt("Font size", &mBitmapTextureSize, 0.2f, 8, 64, "%d px"))
        {
            mBitmapTextureInvalidated = true;
        }

        static size_t sOutputIndex = 0;
        static const std::vector<std::pair<std::string, GlyphShaderBitmap::Output>> sOutputItems =
        {
            { "Alpha Blend", GlyphShaderBitmap::Output::AlphaBlend },
            { "Alpha Test", GlyphShaderBitmap::Output::AlphaTest }
        };
        if (ImGui::BeginCombo("Output", sOutputItems[sOutputIndex].first.c_str()))
        {
            for (size_t lIndex = 0; lIndex < sOutputItems.size(); lIndex++)
            {
                const bool lSelected = (sOutputIndex == lIndex);
                if (ImGui::Selectable(sOutputItems[lIndex].first.c_str(), lSelected))
                {
                    sOutputIndex = lIndex;
                    mBitmapOutput = sOutputItems[lIndex].second;
                }
                if (lSelected)
                {
                    ImGui::SetItemDefaultFocus();
                }
            }
            ImGui::EndCombo();
        }

        static size_t sFilterIndex = 0;
        static const std::vector<std::pair<std::string, GlyphTexture::Filter>> sFilterItems =
        {
            { "Nearest", GlyphTexture::Filter::Nearest },
            { "Linear", GlyphTexture::Filter::Linear}
        };
        if (ImGui::BeginCombo("Filter##Bitmap", sFilterItems[sFilterIndex].first.c_str()))
        {
            for (int lIndex = 0; lIndex < 2; lIndex++)
            {
                const bool lSelected = (sFilterIndex == lIndex);
                if (ImGui::Selectable(sFilterItems[lIndex].first.c_str(), lSelected))
                {
                    sFilterIndex = lIndex;
                    mBitmapFilter = sFilterItems[lIndex].second;
                }
                if (lSelected)
                {
                    ImGui::SetItemDefaultFocus();
                }
            }
            ImGui::EndCombo();
        }
    }

    if (mTechnique == Technique::SDF)
    {
        if (ImGui::DragInt("Mask size", &mSDFTextureSize, 0.2f, 8, 128, "%d px"))
        {
            mSDFTextureInvalidated = true;
        }

        static size_t sFilterIndex = 0;
        static const std::vector<std::pair<std::string, GlyphTexture::Filter>> sFilterItems =
        {
            { "Nearest", GlyphTexture::Filter::Nearest },
            { "Linear", GlyphTexture::Filter::Linear}
        };
        if (ImGui::BeginCombo("Filter##SDF", sFilterItems[sFilterIndex].first.c_str()))
        {
            for (int lIndex = 0; lIndex < 2; lIndex++)
            {
                const bool lSelected = (sFilterIndex == lIndex);
                if (ImGui::Selectable(sFilterItems[lIndex].first.c_str(), lSelected))
                {
                    sFilterIndex = lIndex;
                    mSDFFilter = sFilterItems[lIndex].second;
                }
                if (lSelected)
                {
                    ImGui::SetItemDefaultFocus();
                }
            }
            ImGui::EndCombo();
        }

        ImGui::DragFloat("Edge threshold", &mSDFEdgeThreshold, 0.001f, 0.0f, 1.0f, "%.3f");
        ImGui::DragFloat("Edge softness", &mSDFEdgeSoftness, 0.001f, 0.0f, 1.0f, "%.3f");

        ImGui::Checkbox("Enable Outline", &mSDFEnableOutline);
        ImGui::ColorEdit4("Outline color", glm::value_ptr(mSDFOutlineColor));
        ImGui::DragFloat("Outline threshold", &mSDFOutlineThreshold, 0.001f, 0.0f, 1.0f, "%.3f");
        ImGui::DragFloat("Outline softness", &mSDFOutlineSoftness, 0.001f, 0.0f, 1.0f, "%.3f");
    }

    ImGui::End();
}

int main()
{
    GlyphDemo().Show(1200, 800);
}
