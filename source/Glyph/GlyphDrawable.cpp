#include "GlyphDrawable.hpp"

#include <glad/gl.h>

namespace
{

constexpr GLuint kVertexArrayAttribVertexCoord = 0;
constexpr GLuint kVertexArrayAttribTextureCoord = 1;
constexpr GLuint kVertexBufferBinding = 0;

constexpr GLuint kVertexBufferCount = 16;
constexpr GLuint kElementBufferCount = 6;

}

GlyphDrawable::GlyphDrawable() :
    mVertexArrayHandle(0),
    mVertexBufferHandle(0),
    mElementBufferHandle(0)
{
    glCreateVertexArrays(1, &mVertexArrayHandle);

    glCreateBuffers(1, &mVertexBufferHandle);
    glNamedBufferStorage(mVertexBufferHandle, kVertexBufferCount * sizeof(GLfloat), nullptr, GL_DYNAMIC_STORAGE_BIT);
    glVertexArrayVertexBuffer(mVertexArrayHandle, kVertexBufferBinding, mVertexBufferHandle, 0, 4 * sizeof(GLfloat));
    glVertexArrayBindingDivisor(mVertexArrayHandle, kVertexBufferBinding, 0);

    glEnableVertexArrayAttrib(mVertexArrayHandle, kVertexArrayAttribVertexCoord);
    glVertexArrayAttribFormat(mVertexArrayHandle, kVertexArrayAttribVertexCoord, 2, GL_FLOAT, GL_FALSE, 0);
    glVertexArrayAttribBinding(mVertexArrayHandle, kVertexArrayAttribVertexCoord, kVertexBufferBinding);

    glEnableVertexArrayAttrib(mVertexArrayHandle, kVertexArrayAttribTextureCoord);
    glVertexArrayAttribFormat(mVertexArrayHandle, kVertexArrayAttribTextureCoord, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat));
    glVertexArrayAttribBinding(mVertexArrayHandle, kVertexArrayAttribTextureCoord, kVertexBufferBinding);

    glCreateBuffers(1, &mElementBufferHandle);
    glNamedBufferStorage(mElementBufferHandle, kElementBufferCount * sizeof(GLuint), nullptr, GL_DYNAMIC_STORAGE_BIT);
    glVertexArrayElementBuffer(mVertexArrayHandle, mElementBufferHandle);
}

GlyphDrawable::~GlyphDrawable()
{
    glDeleteVertexArrays(1, &mVertexArrayHandle);
    glDeleteBuffers(1, &mVertexBufferHandle);
    glDeleteBuffers(1, &mElementBufferHandle);
}

std::shared_ptr<GlyphDrawable> GlyphDrawable::Create()
{
    return std::make_shared<GlyphDrawable>();
}

bool GlyphDrawable::IsNull() const
{
    return mVertexArrayHandle == 0;
}

void GlyphDrawable::Draw() const
{
    glBindVertexArray(mVertexArrayHandle);
    glDrawElements(GL_TRIANGLES, kElementBufferCount, GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
}

void GlyphDrawable::SetGeometry(
    int32_t pWidth,
    int32_t pHeight)
{
    const int32_t lOffsetX = -pWidth / 2;
    const int32_t lOffsetY = -pHeight / 2;

    const GLfloat lVertexBufferData[kVertexBufferCount] =
    {
        lOffsetX, lOffsetY, 0.0f, 1.0f,
        lOffsetX + pWidth, lOffsetY, 1.0f, 1.0f,
        lOffsetX, lOffsetY + pHeight, 0.0f, 0.0f,
        lOffsetX + pWidth, lOffsetY + pHeight, 1.0f, 0.0f
    };

    const GLuint lElementBufferData[kElementBufferCount] =
    {
        0, 1, 3, 0, 3, 2
    };

    glNamedBufferSubData(mVertexBufferHandle, 0, kVertexBufferCount * sizeof(GLfloat), lVertexBufferData);
    glNamedBufferSubData(mElementBufferHandle, 0, kElementBufferCount * sizeof(GLuint), lElementBufferData);
}
