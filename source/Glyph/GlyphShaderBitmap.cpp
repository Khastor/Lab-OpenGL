#include "GlyphShaderBitmap.hpp"

#include <glad/gl.h>

GlyphShaderBitmap::GlyphShaderBitmap() :
    GlyphShader("assets/shaders/Glyph/GlyphBitmap") {}

std::shared_ptr<GlyphShaderBitmap> GlyphShaderBitmap::Create()
{
    return std::make_shared<GlyphShaderBitmap>();
}

void GlyphShaderBitmap::SetOutput(
    Output pOutput)
{
    GLint lOutlineLocation = glGetUniformLocation(mHandle, "uOutput");
    glUniform1i(lOutlineLocation, static_cast<GLint>(pOutput));
}
