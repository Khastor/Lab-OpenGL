#include "GlyphTexture.hpp"

#include <ft2build.h>
#include <freetype/freetype.h>

#include <glad/gl.h>

#include <iostream>

namespace
{

constexpr GLint kPixelUnpackAlignmentIgnore = 1;
constexpr GLint kPixelUnpackAlignmentDefault = 4;

}

GlyphTexture::GlyphTexture(
    uint32_t pCodepoint,
    uint32_t pFontHeight,
    const std::string& pFontFile,
    bool pSDF) :
    mHandle(0),
    mWidth(0),
    mHeight(0)
{
    FT_Library lLibrary;
    if (FT_Error lError = FT_Init_FreeType(&lLibrary))
    {
        std::cerr << "FreeType: " << FT_Error_String(lError) << std::endl;
        return;
    }

    FT_Face lFontFace;
    if (FT_Error lError = FT_New_Face(lLibrary, pFontFile.c_str(), 0, &lFontFace))
    {
        std::cerr << "FreeType: " << FT_Error_String(lError) << std::endl;
        return;
    }

    if (FT_Error lError = FT_Select_Charmap(lFontFace, FT_ENCODING_UNICODE))
    {
        std::cerr << "FreeType: " << FT_Error_String(lError) << std::endl;
        return;
    }

    if (FT_Error lError = FT_Set_Pixel_Sizes(lFontFace, 0, pFontHeight))
    {
        std::cerr << "FreeType: " << FT_Error_String(lError) << std::endl;
        return;
    }

    if (FT_Error lError = FT_Load_Char(lFontFace, pCodepoint, FT_LOAD_RENDER))
    {
        std::cerr << "FreeType: " << FT_Error_String(lError) << std::endl;
        return;
    }

    if (pSDF)
    {
        // https://freetype.org/freetype2/docs/reference/ft2-glyph_retrieval.html#ft_render_mode
        if (FT_Error lError = FT_Render_Glyph(lFontFace->glyph, FT_RENDER_MODE_SDF))
        {
            std::cerr << "FreeType: " << FT_Error_String(lError) << std::endl;
        }
    }

    mWidth = lFontFace->glyph->bitmap.width;
    mHeight = lFontFace->glyph->bitmap.rows;

    glCreateTextures(GL_TEXTURE_2D, 1, &mHandle);
    if (mWidth * mHeight > 0)
    {
        glTextureStorage2D(mHandle, 1, GL_R8, mWidth, mHeight);

        const uint8_t* lTexels = lFontFace->glyph->bitmap.buffer;
        glPixelStorei(GL_UNPACK_ALIGNMENT, kPixelUnpackAlignmentIgnore);
        glTextureSubImage2D(mHandle, 0, 0, 0, mWidth, mHeight, GL_RED, GL_UNSIGNED_BYTE, lTexels);
        glPixelStorei(GL_UNPACK_ALIGNMENT, kPixelUnpackAlignmentDefault);
    }
    else
    {
        glTextureStorage2D(mHandle, 1, GL_R8, 1, 1);

        const uint8_t lTexel = 0;
        glPixelStorei(GL_UNPACK_ALIGNMENT, kPixelUnpackAlignmentIgnore);
        glTextureSubImage2D(mHandle, 0, 0, 0, 1, 1, GL_RED, GL_UNSIGNED_BYTE, &lTexel);
        glPixelStorei(GL_UNPACK_ALIGNMENT, kPixelUnpackAlignmentDefault);
    }

    glTextureParameteri(mHandle, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTextureParameteri(mHandle, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glTextureParameteri(mHandle, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTextureParameteri(mHandle, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    FT_Done_Face(lFontFace);
    FT_Done_FreeType(lLibrary);
}

GlyphTexture::~GlyphTexture()
{
    glDeleteTextures(1, &mHandle);
}

std::shared_ptr<GlyphTexture> GlyphTexture::CreateBitmap(
    uint32_t pCodepoint,
    uint32_t pFontHeight,
    const std::string& pFontFile)
{
    return std::make_shared<GlyphTexture>(pCodepoint, pFontHeight, pFontFile, false);
}

std::shared_ptr<GlyphTexture> GlyphTexture::CreateSDF(
    uint32_t pCodepoint,
    uint32_t pFontHeight,
    const std::string& pFontFile)
{
    return std::make_shared<GlyphTexture>(pCodepoint, pFontHeight, pFontFile, true);
}

bool GlyphTexture::IsNull() const
{
    return mHandle == 0;
}

int32_t GlyphTexture::GetWidth() const
{
    return mWidth;
}

int32_t GlyphTexture::GetHeight() const
{
    return mHeight;
}

void GlyphTexture::Bind(
    uint32_t pUnit) const
{
    glBindTextureUnit(pUnit, mHandle);
}

void GlyphTexture::SetFilter(
    Filter pFilter)
{
    switch (pFilter)
    {
    case Filter::Nearest:
        glTextureParameteri(mHandle, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTextureParameteri(mHandle, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        break;
    case Filter::Linear:
        glTextureParameteri(mHandle, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTextureParameteri(mHandle, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        break;
    }
}
