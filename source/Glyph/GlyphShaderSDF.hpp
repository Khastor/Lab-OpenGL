#ifndef GLYPH_SHADER_SDF_HPP_INCLUDED
#define GLYPH_SHADER_SDF_HPP_INCLUDED

#include "GlyphShader.hpp"

#include <glm/glm.hpp>

class GlyphShaderSDF : public GlyphShader
{
public:
    GlyphShaderSDF();

    GlyphShaderSDF(const GlyphShaderSDF&) = delete;
    GlyphShaderSDF& operator=(const GlyphShaderSDF&) = delete;

    GlyphShaderSDF(GlyphShaderSDF&&) = delete;
    GlyphShaderSDF& operator=(GlyphShaderSDF&&) = delete;

    static std::shared_ptr<GlyphShaderSDF> Create();

    void SetEdgeThreshold(
        float pThreshold);

    void SetEdgeSoftness(
        float pSoftness);

    void SetOutlineEnabled(
        bool pEnabled);

    void SetOutlineThreshold(
        float pThreshold);

    void SetOutlineSoftness(
        float pSoftness);

    void SetOutlineColor(
        const glm::vec4& pColor);
};

#endif // GLYPH_SHADER_SDF_HPP_INCLUDED
