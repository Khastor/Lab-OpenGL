#ifndef GLYPH_SHADER_BITMAP_HPP_INCLUDED
#define GLYPH_SHADER_BITMAP_HPP_INCLUDED

#include "GlyphShader.hpp"

class GlyphShaderBitmap : public GlyphShader
{
public:
    GlyphShaderBitmap();

    GlyphShaderBitmap(const GlyphShaderBitmap&) = delete;
    GlyphShaderBitmap& operator=(const GlyphShaderBitmap&) = delete;

    GlyphShaderBitmap(GlyphShaderBitmap&&) = delete;
    GlyphShaderBitmap& operator=(GlyphShaderBitmap&&) = delete;

    static std::shared_ptr<GlyphShaderBitmap> Create();

    enum class Output
    {
        AlphaBlend,
        AlphaTest
    };

    void SetOutput(
        Output pOutput);
};

#endif // GLYPH_SHADER_BITMAP_HPP_INCLUDED
