#ifndef GLYPH_DEMO_HPP_INCLUDED
#define GLYPH_DEMO_HPP_INCLUDED

#include "GlyphShaderBitmap.hpp"
#include "GlyphTexture.hpp"

#include <CG/Platform/OpenGLWindow.hpp>

#include <glm/glm.hpp>

#include <cstdint>

class GlyphDrawable;
class GlyphShaderSDF;

class GlyphDemo : public CG::Platform::OpenGLWindow
{
public:
    GlyphDemo();

protected:
    bool OnCreate() override;

    bool OnUpdate(
        double pCurrentTime,
        double pElapsedTime) override;

    void OnDestroy() override;

    void OnKeyPressed(
        CG::Platform::OpenGLWindow::KeyboardKey pKey,
        CG::Platform::OpenGLWindow::KeyboardMods pMods) override;

private:
    void ProcessUI();

    enum class Technique
    {
        Bitmap,
        SDF
    };

    Technique mTechnique;

    uint32_t mCodepoint;
    glm::vec4 mColor;
    glm::mat4 mTransform;

    bool mBitmapTextureInvalidated;
    GlyphTexture::Filter mBitmapFilter;
    GlyphShaderBitmap::Output mBitmapOutput;
    int mBitmapTextureSize;

    bool mSDFTextureInvalidated;
    GlyphTexture::Filter mSDFFilter;
    int mSDFTextureSize;
    float mSDFEdgeThreshold;
    float mSDFEdgeSoftness;
    bool mSDFEnableOutline;
    float mSDFOutlineThreshold;
    float mSDFOutlineSoftness;
    glm::vec4 mSDFOutlineColor;

    std::shared_ptr<GlyphDrawable> mBitmapDrawable;
    std::shared_ptr<GlyphShaderBitmap> mBitmapShader;
    std::shared_ptr<GlyphTexture> mBitmapTexture;

    std::shared_ptr<GlyphDrawable> mSDFDrawable;
    std::shared_ptr<GlyphShaderSDF> mSDFShader;
    std::shared_ptr<GlyphTexture> mSDFTexture;
};

#endif // GLYPH_DEMO_HPP_INCLUDED
