#include "GlyphShaderSDF.hpp"

#include <glad/gl.h>

#include <glm/gtc/type_ptr.hpp>

GlyphShaderSDF::GlyphShaderSDF() :
    GlyphShader("assets/shaders/Glyph/GlyphSDF") {}

std::shared_ptr<GlyphShaderSDF> GlyphShaderSDF::Create()
{
    return std::make_shared<GlyphShaderSDF>();
}

void GlyphShaderSDF::SetEdgeThreshold(
    float pThreshold)
{
    GLint lEdgeThresholdLocation = glGetUniformLocation(mHandle, "uEdgeThreshold");
    glUniform1f(lEdgeThresholdLocation, static_cast<GLfloat>(pThreshold));
}

void GlyphShaderSDF::SetEdgeSoftness(
    float pSoftness)
{
    GLint lEdgeSoftnessLocation = glGetUniformLocation(mHandle, "uEdgeSoftness");
    glUniform1f(lEdgeSoftnessLocation, static_cast<GLfloat>(pSoftness));
}

void GlyphShaderSDF::SetOutlineEnabled(
    bool pEnabled)
{
    GLint lOutlineEnabledLocation = glGetUniformLocation(mHandle, "uOutlineEnabled");
    glUniform1i(lOutlineEnabledLocation, static_cast<GLint>(pEnabled));
}

void GlyphShaderSDF::SetOutlineThreshold(
    float pThreshold)
{
    GLint lOutlineThresholdLocation = glGetUniformLocation(mHandle, "uOutlineThreshold");
    glUniform1f(lOutlineThresholdLocation, static_cast<GLfloat>(pThreshold));
}

void GlyphShaderSDF::SetOutlineSoftness(
    float pSoftness)
{
    GLint lOutlineSoftnessLocation = glGetUniformLocation(mHandle, "uOutlineSoftness");
    glUniform1f(lOutlineSoftnessLocation, static_cast<GLfloat>(pSoftness));
}

void GlyphShaderSDF::SetOutlineColor(
    const glm::vec4& pColor)
{
    GLint lOutlineColorLocation = glGetUniformLocation(mHandle, "uOutlineColor");
    glUniform4fv(lOutlineColorLocation, 1, glm::value_ptr(pColor));
}
