#version 460 core

layout (location = 0) in vec4 aVertexCoord;
layout (location = 1) in vec4 aTextureCoord;

out vec2 gTextureCoord;

uniform mat4 uTransform;
uniform mat4 uProjection;

void main()
{
    gl_Position = uProjection * uTransform * vec4(aVertexCoord.xy, 0.0, 1.0);
    gTextureCoord = aTextureCoord.xy;
}
