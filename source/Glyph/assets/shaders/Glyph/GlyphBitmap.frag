#version 460 core

in vec2 gTextureCoord;
out vec4 gFragmentColor;

uniform int uOutput;
uniform vec4 uColor;

layout (binding = 0) uniform sampler2D uTexture;

const int kOutlineAlphaBlend = 0;
const int kOutlineAlphaTest = 1;

void WriteOutlineAlphaBlend()
{
    float lSample = texture(uTexture, gTextureCoord).r;
    gFragmentColor = uColor * vec4(1.0, 1.0, 1.0, lSample);
}

void WriteOutlineAlphaTest()
{
    float lSample = texture(uTexture, gTextureCoord).r;
    gFragmentColor = uColor;
    if (lSample < 0.5)
    {
        discard;
    }
}

void main()
{
    gFragmentColor = vec4(1.0, 1.0, 1.0, 1.0);
    switch (uOutput)
    {
    case kOutlineAlphaBlend:
        WriteOutlineAlphaBlend();
        break;
    case kOutlineAlphaTest:
        WriteOutlineAlphaTest();
        break;
    }
}
