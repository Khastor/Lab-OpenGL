#version 460 core

in vec2 gTextureCoord;
out vec4 gFragmentColor;

uniform vec4 uColor;
uniform float uEdgeThreshold;
uniform float uEdgeSoftness;
uniform int uOutlineEnabled;
uniform float uOutlineThreshold;
uniform float uOutlineSoftness;
uniform vec4 uOutlineColor;

layout (binding = 0) uniform sampler2D uTexture;

void main()
{
    float lDistance = texture(uTexture, gTextureCoord).r;
    if (uOutlineEnabled != 0)
    {
        float lOutlineBlend = smoothstep(uOutlineThreshold - uOutlineSoftness, uOutlineThreshold + uOutlineSoftness, lDistance);
        gFragmentColor = mix(uOutlineColor, uColor, lOutlineBlend);
    }
    else
    {
        gFragmentColor = uColor;
    }
    float lEdgeBlend = smoothstep(uEdgeThreshold - uEdgeSoftness, uEdgeThreshold + uEdgeSoftness, lDistance);
    gFragmentColor = vec4(gFragmentColor.rgb, gFragmentColor.a * lEdgeBlend);
}
