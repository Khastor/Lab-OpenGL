#ifndef GLYPH_SHADER_HPP_INCLUDED
#define GLYPH_SHADER_HPP_INCLUDED

#include <glm/glm.hpp>

#include <cstdint>
#include <memory>
#include <string>

class GlyphShader
{
public:
    virtual ~GlyphShader();

    GlyphShader(const GlyphShader&) = delete;
    GlyphShader& operator=(const GlyphShader&) = delete;

    GlyphShader(GlyphShader&&) = delete;
    GlyphShader& operator=(GlyphShader&&) = delete;

    bool IsNull() const;

    void Use() const;

    void SetColor(
        const glm::vec4& pColor);

    void SetTransform(
        const glm::mat4& pTransform);

    void SetProjection(
        const glm::mat4& pProjection);

protected:
    GlyphShader(
        const std::string& pFilename);

    uint32_t mHandle;
};

#endif // GLYPH_SHADER_HPP_INCLUDED
