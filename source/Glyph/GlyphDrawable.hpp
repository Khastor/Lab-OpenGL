#ifndef GLYPH_DRAWABLE_HPP_INCLUDED
#define GLYPH_DRAWABLE_HPP_INCLUDED

#include <cstdint>
#include <memory>

class GlyphDrawable
{
public:
    GlyphDrawable();

    ~GlyphDrawable();

    GlyphDrawable(const GlyphDrawable&) = delete;
    GlyphDrawable& operator=(const GlyphDrawable&) = delete;

    GlyphDrawable(GlyphDrawable&&) = delete;
    GlyphDrawable& operator=(GlyphDrawable&&) = delete;

    static std::shared_ptr<GlyphDrawable> Create();

    bool IsNull() const;

    void Draw() const;

    void SetGeometry(
        int32_t pWidth,
        int32_t pHeight);

private:
    uint32_t mVertexArrayHandle;
    uint32_t mVertexBufferHandle;
    uint32_t mElementBufferHandle;
};

#endif // GLYPH_DRAWABLE_HPP_INCLUDED
