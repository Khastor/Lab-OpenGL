# Glyph

This project demonstrates how to render a single glyph using a font texture generated at runtime.

## Settings

### Unicode

Set the Unicode code point of the character to render.

### Color

Set the color of the rendered glyph.

### Scale

Set the scale of the rendered glyph.

The scale is applied to the rendered glyph and has no effect on the texture.

### Rotation

Set the rotation of the rendered glyph.

The rotation is applied to the rendered glyph and has no effect on the texture.

### Bitmap Settings

#### Font Size

Set the font size.

The font size is the size of the bitmap texture in pixels.

#### Output

Set the output mode.

* Alpha Blend: use the bitmap value as a blending factor.
* Alpha Test: use the bitmap value as a threshold.

#### Filter

Set the minifying and magnifying filters used for sampling the bitmap texture.

* Nearest: use the nearest texel value.
* Linear: use the weighted average of the 4 nearest texel values.

### Signed Distance Field Settings

#### Mask size

Set the signed distance field texture size.

#### Filter

Set the minifying and magnifying filters used for sampling the signed distance field texture.

* Nearest: use the nearest texel value.
* Linear: use the weighted average of the 4 nearest texel values.

#### Edge threshold

Set the edge threshold.

The edge threshold is a normalized distance value between 0 and 1. The nominal value is 0.5.
Distance values greater than the edge threshold are inside the glyph shape.

#### Edge softness

Set the edge softness.

#### Enable outline

Enable drawing an inner outline.

#### Outline threshold

Set the outline threshold.

The outline threshold is a normalized distance value between 0 and 1.
Distance values between the edge threshold and the outline threshold are inside the outline.

#### Outline softness

Set the outline softness.

#### Outline color

Set the outline color.

## Remarks

When there is no scaling or rotation, the bitmap technique with alpha blending produces decent results even with a small font size.

The signed distance field technique should be preferred when the glyph can be scaled or rotated, and allows implementing effects such as outlines and shadows.

## Screenshots

### Bitmap

![glyph-bitmap-01.png](media/glyph-bitmap-01.png)

![glyph-bitmap-02.png](media/glyph-bitmap-02.png)

![glyph-bitmap-03.png](media/glyph-bitmap-03.png)

![glyph-bitmap-04.png](media/glyph-bitmap-04.png)

### Signed Distance Field

![glyph-sdf-01.png](media/glyph-sdf-01.png)
