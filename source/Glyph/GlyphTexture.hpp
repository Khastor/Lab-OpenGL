#ifndef GLYPH_TEXTURE_HPP_INCLUDED
#define GLYPH_TEXTURE_HPP_INCLUDED

#include <cstdint>
#include <memory>
#include <string>

class GlyphTexture
{
public:
    GlyphTexture(
        uint32_t pCodepoint,
        uint32_t pFontHeight,
        const std::string& pFontFile,
        bool pSDF);

    ~GlyphTexture();

    GlyphTexture(const GlyphTexture&) = delete;
    GlyphTexture& operator=(const GlyphTexture&) = delete;

    GlyphTexture(GlyphTexture&&) = delete;
    GlyphTexture& operator=(GlyphTexture&&) = delete;

    static std::shared_ptr<GlyphTexture> CreateBitmap(
        uint32_t pCodepoint,
        uint32_t pFontHeight,
        const std::string& pFontFile);

    static std::shared_ptr<GlyphTexture> CreateSDF(
        uint32_t pCodepoint,
        uint32_t pFontHeight,
        const std::string& pFontFile);

    bool IsNull() const;

    void Bind(
        uint32_t pUnit) const;

    int32_t GetWidth() const;

    int32_t GetHeight() const;

    enum class Filter
    {
        Nearest,
        Linear
    };

    void SetFilter(
        Filter pFilter);

private:
    uint32_t mHandle;
    int32_t mWidth;
    int32_t mHeight;
};

#endif // GLYPH_TEXTURE_HPP_INCLUDED
