﻿cmake_minimum_required (VERSION 3.28)
project (LabCGOpenGL)

set (CMAKE_CXX_STANDARD 20)
set (CMAKE_CXX_STANDARD_REQUIRED ON)

###############################################################################
# Configure submodules                                                        #
###############################################################################

# Configure Lab-CG-Common
add_subdirectory ("submodules/Lab-CG-Common")

###############################################################################
# Configure Lab-CG-OpenGL                                                     #
###############################################################################

# Configure Glyph
add_subdirectory (source/Glyph)

# Install shared assets
install (DIRECTORY assets TYPE BIN)
