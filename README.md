# Lab-CG-OpenGL

Computer Graphics Lab for experimenting with OpenGL.

## Submodules

This repository depends on submodules. Don't forget to clone it with the `--recurse-submodules` flag.

* [Lab-CG-Common](https://gitlab.com/Khastor/Lab-CG-Common)

## Build with CMake

```
$ mkdir build
$ cd build
$ cmake ..
$ cmake --build .
$ cmake --install .
```

## Content

### Glyph

This project demonstrates how to render a single glyph using a font texture generated at runtime.

## Resources

* [Learn OpenGL](https://learnopengl.com/)
* [OpenGL Wiki](https://www.khronos.org/opengl/wiki/)
* [docs.gl](https://docs.gl/)
